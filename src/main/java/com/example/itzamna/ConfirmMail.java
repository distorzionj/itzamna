package com.example.itzamna;

import com.example.itzamna.dao.FormTokenRepository;
import com.example.itzamna.dao.NucleoAgrarioRepository;
import com.example.itzamna.model.FormToken;
import com.example.itzamna.model.FormTokenType;
import com.example.itzamna.model.NucleoAgrario;
import com.example.itzamna.service.MailService;
import com.example.itzamna.service.dto.EmailDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
@Transactional
public class ConfirmMail implements ApplicationListener<ApplicationReadyEvent> {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${populator.process.confirm-mail}")
    private boolean processFlag;

    @Autowired
    NucleoAgrarioRepository nucleoAgrarioRepository;

    @Autowired
    FormTokenRepository formTokenRepository;

    @Autowired
    MailService mailService;


    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        if(processFlag){
            String row;
            try {
                BufferedReader csvReader = new BufferedReader(new FileReader(new ClassPathResource("document_control_data.csv").getFile()));
                while ((row = csvReader.readLine()) != null) {
                    String[] data = row.split(";");
                    //LOGGER.debug(row);
                    Optional<NucleoAgrario> nucleoAgrario = nucleoAgrarioRepository.findById(Integer.valueOf(data[0]));
                    if(nucleoAgrario.isPresent()){
                        List<FormToken> nucleoFormTokens = formTokenRepository.findAllByNucleoAgrarioId(nucleoAgrario.get().getId());
                        Optional<FormToken> token = nucleoFormTokens.stream().filter(formToken -> formToken.getType().equals(FormTokenType.CONFIRM_MAIL)).findFirst();
                        if(data.length >= 10 && data[9].length() > 0 && mailService.isValidEmail(data[9]) && !token.isPresent()){
                            FormToken newToken = new FormToken(FormTokenType.CONFIRM_MAIL);
                            newToken.setEmail(data[9]);
                            newToken.setNucleoAgrario(nucleoAgrario.get());
                            formTokenRepository.save(newToken);

                            //sendConfirmationMail(newToken, data);
                            LOGGER.info("Sending email to " + newToken.getEmail() + " for " + newToken.getNucleoAgrario().getMunicipality().getName() + " -> " + newToken.getNucleoAgrario().getName());
                        } else if(token.isPresent()){
                            if(token.get().isMailed()) LOGGER.debug("Email already sent for " + nucleoAgrario.get().getMunicipality().getName() + " -> " + nucleoAgrario.get().getName());
                            else {
                                LOGGER.info("Retrying to send email to " + token.get().getEmail() + " for " + token.get().getNucleoAgrario().getMunicipality().getName() + " -> " + token.get().getNucleoAgrario().getName());
                                sendConfirmationMail(token.get(), data);
                            }
                        }
                        else LOGGER.debug("Email is invalid/not present for " + nucleoAgrario.get().getMunicipality().getName() + " -> " + nucleoAgrario.get().getName());

                    }
                    else LOGGER.debug("nucleo agrario not found");
                }
                csvReader.close();
            } catch (IOException ex){
                LOGGER.debug("IO Exception");
            }
        }
    }

    private void sendConfirmationMail(FormToken token, String[] mailData){
        EmailDTO mail = new EmailDTO();

        mail.setMailFrom("fondo.mixto.pa@gmail.com");
        mail.setMailTo(token.getEmail());
        mail.setMailSubject("Fondo Mixto Privado - Confirmación de Correo");
        String mailContent = new StringBuilder()
                .append("Hola,<br /><br />")
                .append("Este es un correo de bienvenida para el Fondo Mixto Privado de los Pueblos originarios.")
                .append("Para nosotros, es importante que tu conozcas la información de tu ejido, por lo que nos permitimos comentarte lo siguiente:<br /><br />")
                .append("<b>Tu municipio:</b> " + token.getNucleoAgrario().getMunicipality().getState().getName() + "<br />")
                .append("<b>Tu municipio:</b> " + token.getNucleoAgrario().getMunicipality().getName() + "<br />")
                .append("<b>Tu ejido:</b> " + token.getNucleoAgrario().getName() + "<br />")
                .append("<b>Tenemos su resolucion presidencial?</b> " + (mailData[4].equals("1") ? "Si" : "No") + "<br />")
                .append("<b>Tenemos su acta de posesion?</b> " + (mailData[5].equals("1") ? "Si" : "No") + "<br />")
                .append("<b>Tenemos su plano?</b> " + (mailData[6].equals("1") ? "Si" : "No") + "<br />")
                .append("<b>Tenemos su diario oficial?</b> " + (mailData[7].equals("1") ? "Si" : "No") + "<br />")
                .append("<b>Tenemos su ficha tecnica?</b> " + (mailData[8].equals("1") ? "Si" : "No") + "<br /><br />")
                .append("Si alguna documentación estuviera marcada con <b>NO</b> quiere decir que tendrías que conseguirla con el comisario ejidal y reportarlo con tu monitor de grupo")
                .append("La información que se necesita recabar es la siguiente:<br />")
                .append("Datos del comisario: Nombre completo, fecha de nacimiento, teléfono, dirección, correo electrónico (si tiene), fecha de inicio y fecha de conclusión de gestión<br />")
                .append("Datos del enlace (Tu): Nombre completo, fecha de nacimiento, télefono, dirección y correo electrónico (necesario)<br />")
                .append("En el caso de observaciones de las autoridades ejidales, reportarlo con tu monitor de grupo")
                .append("Igual, necesitamos que confirmes tu correo electrónico, dando click en <a href='http://itzamna.appspot.com/form/mailconfirm/" + token.getId().toString() + "'>http://itzamna.appspot.com/form/mailconfirm/" + token.getId().toString() + "</a><br /><br />")
                .append("una vez confirmado el correo electrónico, recibirás a la brevedad los formatos e instrucciones de captura.")
                .append("Si el correo presenta algún error en los datos proporcionados, favor de reportarlo")
                .append("<br /><br />Si tu no solicitaste este correo, por favor, ignóralo")
                .toString();
        mail.setMailContent(mailContent);
        try{
            mailService.sendEmail(mail);
            token.setMailed(true);
            formTokenRepository.save(token);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}