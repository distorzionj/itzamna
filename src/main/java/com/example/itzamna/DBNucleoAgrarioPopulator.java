package com.example.itzamna;

import com.example.itzamna.dao.MunicipalityRepository;
import com.example.itzamna.dao.NucleoAgrarioRepository;
import com.example.itzamna.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

@Component
@Transactional
public class DBNucleoAgrarioPopulator implements ApplicationListener<ApplicationReadyEvent> {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private NucleoAgrarioRepository nucleoAgrarioRepository;

    @Autowired
    private MunicipalityRepository municipalityRepository;

    @Value("${populator.process.nucleos_agrarios}")
    private boolean processFlag;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        if(processFlag){
            LOGGER.debug("Processing nucleos agrarios to add");
            String row;
            try {
                BufferedReader csvReader = new BufferedReader(new FileReader(new ClassPathResource("nucleos_agrarios.to.add.csv").getFile()));
                while ((row = csvReader.readLine()) != null && row.length() > 0) {
                    String[] data = row.split(";");
                    boolean isAdded = Boolean.valueOf(data[2]);
                    if(!isAdded) {
                        //LOGGER.debug(row);
                        Optional<Municipality> municipality = municipalityRepository.findById(Integer.valueOf(data[0]));
                        if (municipality.isPresent()) {
                            NucleoAgrario nucleoAgrario = new NucleoAgrario();
                            nucleoAgrario.setName(data[1]);
                            nucleoAgrario.setMunicipality(municipality.get());
                            nucleoAgrario.setPriority(Integer.valueOf(data[3]) != 0);
                            nucleoAgrario.setNucleoAgrarioType(NucleoAgrarioType.NINGUNA);
                            nucleoAgrario.setTipo(TipoNucleo.EJIDO);
                            nucleoAgrarioRepository.save(nucleoAgrario);
                            LOGGER.debug("New nucleo_agrario added to [" + municipality.get().getName() + "]: " + nucleoAgrario.getName() + "(" + nucleoAgrario.getId() + ")");
                        } else {
                            LOGGER.debug("State not found id: " + data[0] + " | Row: " + row);
                        }
                    }
                }
                csvReader.close();
            } catch (IOException ex){
                LOGGER.debug("IO Exception");
            }
        }
    }

}