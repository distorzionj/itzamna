package com.example.itzamna;

import com.example.itzamna.dao.MunicipalityRepository;
import com.example.itzamna.dao.NucleoAgrarioRepository;
import com.example.itzamna.dao.StateRepository;
import com.example.itzamna.model.Municipality;
import com.example.itzamna.model.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

@Component
@Transactional
public class DBMunicipalityPopulator implements ApplicationListener<ApplicationReadyEvent> {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private MunicipalityRepository municipalityRepository;

    @Value("${populator.process.municipalities}")
    private boolean processFlag;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        if(processFlag){
            LOGGER.debug("Processing municipalities to add");
            String row;
            try {
                BufferedReader csvReader = new BufferedReader(new FileReader(new ClassPathResource("municipalities.to.add.csv").getFile()));
                while ((row = csvReader.readLine()) != null && row.length() > 0) {
                    String[] data = row.split(";");
                    boolean isAdded = Boolean.valueOf(data[2]);
                    if(!isAdded) {
                        //LOGGER.debug(row);
                        Optional<State> state = stateRepository.findById(Integer.valueOf(data[0]));
                        if (state.isPresent()) {
                            Municipality municipality = new Municipality();
                            municipality.setName(data[1]);
                            municipality.setState(state.get());
                            municipalityRepository.save(municipality);
                            LOGGER.debug("New municipality added to [" + state.get().getName() + "]: " + municipality.getName() + "(" + municipality.getId() + ")");
                        } else {
                            LOGGER.debug("State not found id: " + data[0] + " | Row: " + row);
                        }
                    }
                }
                csvReader.close();
            } catch (IOException ex){
                LOGGER.debug("IO Exception");
            }
        }
    }

}