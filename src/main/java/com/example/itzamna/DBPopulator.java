package com.example.itzamna;

import com.example.itzamna.dao.NucleoAgrarioRepository;
import com.example.itzamna.model.NucleoAgrario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Component
@Transactional
public class DBPopulator implements ApplicationListener<ApplicationReadyEvent> {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private boolean processFlag = false;

    @Autowired
    private NucleoAgrarioRepository nucleoAgrarioRepository;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        if(processFlag){
            String row;
            try {
                BufferedReader csvReader = new BufferedReader(new FileReader(new ClassPathResource("document_control_data.csv").getFile()));
                while ((row = csvReader.readLine()) != null) {
                    String[] data = row.split(";");
                    LOGGER.debug(row);
                    NucleoAgrario nucleoAgrario = nucleoAgrarioRepository.getOne(Integer.valueOf(data[0]));
                    if(nucleoAgrario != null){
                        LOGGER.debug("Municipio: " + nucleoAgrario.getMunicipality().getName() + "| Ejido: " + nucleoAgrario.getName());
                    }
                    else LOGGER.debug("nucleo agrario not found");
                }
                csvReader.close();
            } catch (IOException ex){
                LOGGER.debug("IO Exception");
            }
        }
    }

}