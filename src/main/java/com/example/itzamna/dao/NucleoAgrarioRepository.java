package com.example.itzamna.dao;

import com.example.itzamna.model.NucleoAgrario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NucleoAgrarioRepository extends JpaRepository<NucleoAgrario, Integer> {
}
