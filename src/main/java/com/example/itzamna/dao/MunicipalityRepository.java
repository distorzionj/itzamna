package com.example.itzamna.dao;

import com.example.itzamna.model.Municipality;
import com.example.itzamna.model.NucleoAgrario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MunicipalityRepository extends JpaRepository<Municipality, Integer> {
}
