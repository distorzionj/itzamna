package com.example.itzamna.dao;

import com.example.itzamna.model.FormToken;
import com.example.itzamna.model.FormTokenType;
import org.hibernate.annotations.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.text.Normalizer;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface FormTokenRepository extends JpaRepository<FormToken, UUID> {

    List<FormToken> findAllByNucleoAgrarioId(int id);
    List<FormToken> findAllByType(FormTokenType type);
    List<FormToken> findAllByTypeAndMailed(FormTokenType type, boolean mailed);


    Optional<FormToken> findOneByEmailAndType(String email, FormTokenType type);
}
