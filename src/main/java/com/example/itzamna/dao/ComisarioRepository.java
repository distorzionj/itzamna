package com.example.itzamna.dao;

import com.example.itzamna.model.Comisario;
import com.example.itzamna.model.NucleoAgrario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComisarioRepository extends JpaRepository<Comisario, Long> {

    Comisario findByNucleoAgrario(NucleoAgrario nucleoAgrario);

}
