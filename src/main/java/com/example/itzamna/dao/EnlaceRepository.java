package com.example.itzamna.dao;

import com.example.itzamna.model.Enlace;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnlaceRepository extends JpaRepository<Enlace, Long> {
}
