package com.example.itzamna.model;

public enum TipoDocumento {

    RESOLUCION      (0,"Resolucion Presidencial"),
    APD             (1,"Acta de Posesion y Deslinde"),
    PLANO           (2,"Plano"),
    DIARIO          (3,"Diario Oficial"),
    FICHA           (4,"Ficha Tecnica");

    private int value;
    private String text;
    private TipoDocumento(int value, String text){
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }
}
