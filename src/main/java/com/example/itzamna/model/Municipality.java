package com.example.itzamna.model;

import com.example.itzamna.service.validators.IfPresentNumber;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="municipality")
public class Municipality {

    public Municipality() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "state_id", nullable = false)
    private State state;

    private String name;

    public Municipality(String name, State state) {
        this.name = name;
        this.state = state;
    }

    @OneToMany(mappedBy = "municipality", cascade = CascadeType.ALL)
    private Set<NucleoAgrario> nucleos = new HashSet<>();

    @OneToMany(mappedBy = "municipality", cascade = CascadeType.ALL)
    @OrderBy("fecha_inicio DESC")
    private Set<MunicipalityPresident> presidents = new HashSet<>();

    private String RFC;

    private String email;

    //GETTERS AND SETTERS

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<NucleoAgrario> getNucleos() {
        return nucleos;
    }

    public void setNucleos(Set<NucleoAgrario> nucleos) {
        this.nucleos = nucleos;
    }

    public Set<MunicipalityPresident> getPresidents() {
        return presidents;
    }

    public void setPresidents(Set<MunicipalityPresident> presidents) {
        this.presidents = presidents;
    }

    public boolean currentPresidentHasInfo(){
        Set<MunicipalityPresident> allPresidents = getPresidents();
        if(allPresidents.isEmpty()) return false;
        else{
            MunicipalityPresident lastPresident = allPresidents.stream().iterator().next();
            if(lastPresident.getFechaFinal().compareTo(new Date()) >= 0){
                return true;
            }
            else return false;
        }
    }


}
