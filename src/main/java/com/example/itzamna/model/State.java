package com.example.itzamna.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="state")
public class State {

    public State() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name="country_id", nullable = false)
    private Country country;

    @OneToMany(mappedBy = "state", cascade = CascadeType.ALL)
    private Set<Municipality> municipalities = new HashSet<>();

    private String name;

    public State(String name, Country country) {
        this.name = name;
        this.country = country;
    }

    //GETTERS AND SETTERS
    public int getId() {
        return id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Municipality> getMunicipalities() {
        return municipalities;
    }
}
