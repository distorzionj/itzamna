package com.example.itzamna.model;

public enum RoleType {
    ENLACE              (Values.COMISARIO),
    COMISARIO           (Values.COMISARIO),
    MUNICIPALITY_PRESIDENT           (Values.MUNICIPALITY_PRESIDENT);

    private String value;

    private RoleType(String value) {
        this.value = value;
    }

    public static class Values {
        public static final String ENLACE = "E";
        public static final String COMISARIO = "C";
        public static final String MUNICIPALITY_PRESIDENT = "MP";
    }

}
