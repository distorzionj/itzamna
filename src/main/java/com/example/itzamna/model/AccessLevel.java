package com.example.itzamna.model;

public enum AccessLevel {
    USER            (0),
    API_USER        (1),
    STATE_ADMIN     (79),
    REGION_ADMIN    (89),
    GLOBAL_ADMIN    (99),
    WEB_MASTER      (100);

    private int value;
    private AccessLevel(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
