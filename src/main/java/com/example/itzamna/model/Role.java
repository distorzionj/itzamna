package com.example.itzamna.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@DiscriminatorColumn(name="role_type", discriminatorType = DiscriminatorType.STRING)
@Table(name = "role")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    @Column(name="role_type", insertable = false, updatable = false)
    private String roleType;

    public String getRoleType() {
        return roleType;
    }

    //public abstract Set<NucleoAgrario> hasAccess();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
