package com.example.itzamna.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
public class FormToken {

    @Id
    @GeneratedValue
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID id;

    @NotNull
    private String email;

    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    private NucleoAgrario nucleoAgrario;

    @Column(columnDefinition = "int not null default 0")
    private boolean mailed;

    @Column(columnDefinition = "int not null")
    private FormTokenType type;

    public FormToken(FormTokenType type) {
        this.type = type;
    }

    public FormToken() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public NucleoAgrario getNucleoAgrario() {
        return nucleoAgrario;
    }

    public void setNucleoAgrario(NucleoAgrario nucleoAgrario) {
        this.nucleoAgrario = nucleoAgrario;
    }

    public boolean isMailed() {
        return mailed;
    }

    public void setMailed(boolean mailed) {
        this.mailed = mailed;
    }

    public FormTokenType getType() {
        return type;
    }

    public void setType(FormTokenType type) {
        this.type = type;
    }
}
