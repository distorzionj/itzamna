package com.example.itzamna.model;

public enum TipoNucleo {
    EJIDO       (0),
    NCPA        (1);

    private int value;
    private TipoNucleo(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
