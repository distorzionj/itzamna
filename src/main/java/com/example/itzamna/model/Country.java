package com.example.itzamna.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="country")
public class Country {

    public Country() {
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    private String name;

    @OneToMany(mappedBy="country", cascade = CascadeType.ALL)
    private Set<State> states = new HashSet<>();

    public Country(String name) {
        this.name = name;
    }

    //GETTERS AND SETTERS
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public Set<State> getStates() {
        return states;
    }
}
