package com.example.itzamna.model;

public enum FormTokenType {

    CONFIRM_MAIL (0),
    CAPTURE_FORM_ONE (1),
    CAPTURE_FORM_INE (2),
    NEXT_STEP (3);

    private int value;
    private FormTokenType(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
