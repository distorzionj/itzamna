package com.example.itzamna.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.Set;

@Entity
@DiscriminatorValue(value=RoleType.Values.ENLACE)
public class Enlace extends Role {

    @OneToOne
    @JoinColumn(name = "nucleoAgrario_id", nullable = true)
    private NucleoAgrario nucleoAgrario;

    public Enlace() {
    }

    public NucleoAgrario getNucleoAgrario() {
        return nucleoAgrario;
    }

    public void setNucleoAgrario(NucleoAgrario nucleoAgrario) {
        this.nucleoAgrario = nucleoAgrario;
    }
}
