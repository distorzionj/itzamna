package com.example.itzamna.model;

public enum AccionNucleo {
    NONE            (0),
    DOTACION        (1);

    private int value;
    private AccionNucleo(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
