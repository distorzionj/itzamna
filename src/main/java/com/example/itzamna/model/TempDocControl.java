package com.example.itzamna.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TempDocControl {

    @Id
    @GeneratedValue
    private int id;

    @Column(columnDefinition = "int not null default 0")
    private boolean rp;

    @Column(columnDefinition = "int not null default 0")
    private boolean apd;

    @Column(columnDefinition = "int not null default 0")
    private boolean plano;

    @Column(columnDefinition = "int not null default 0")
    private boolean dof;

    @Column(columnDefinition = "int not null default 0")
    private boolean ft;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isRp() {
        return rp;
    }

    public void setRp(boolean rp) {
        this.rp = rp;
    }

    public boolean isApd() {
        return apd;
    }

    public void setApd(boolean apd) {
        this.apd = apd;
    }

    public boolean isPlano() {
        return plano;
    }

    public void setPlano(boolean plano) {
        this.plano = plano;
    }

    public boolean isDof() {
        return dof;
    }

    public void setDof(boolean dof) {
        this.dof = dof;
    }

    public boolean isFt() {
        return ft;
    }

    public void setFt(boolean ft) {
        this.ft = ft;
    }
}
