package com.example.itzamna.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //@NotBlank(message = "firstName required")
    private String firstName;

    //@NotBlank(message = "lastName required")
    private String lastName;

    //@NotBlank(message = "email required")
    @Column(unique = true)
    private String email;

    //@NotBlank(message = "password required")
    private String password;

    //@Past(message = "birthday has to be a past date")
    //@JsonFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    //@NotBlank(message = "birthday required")
    private Date birthday;

    @OneToMany(mappedBy="user", cascade = CascadeType.ALL)
    private Set<Role> roles = new HashSet<>();

    private String phone;

    private String address;

    private String ineFrente;

    private String ineReverso;

    public User() {
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void addRole(Role role) { this.roles.add(role); }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIneFrente() {
        return ineFrente;
    }

    public void setIneFrente(String ineFrente) {
        this.ineFrente = ineFrente;
    }

    public String getIneReverso() {
        return ineReverso;
    }

    public void setIneReverso(String ineReverso) {
        this.ineReverso = ineReverso;
    }
}
