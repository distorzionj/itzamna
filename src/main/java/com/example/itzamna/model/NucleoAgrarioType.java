package com.example.itzamna.model;

public enum NucleoAgrarioType {
    NINGUNA (0);

    private int value;
    private NucleoAgrarioType(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
