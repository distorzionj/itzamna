package com.example.itzamna.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "nucleoAgrario")
public class NucleoAgrario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "municipality_id", nullable = false)
    private Municipality municipality;

    @Column(nullable = true)
    private String claveUnica;

    @Column(nullable = true)
    private String folioMatriz;

    @Column(nullable = true)
    @Basic
    @JsonFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    private java.util.Date fechaInscripcion;

    @Column(columnDefinition = "float not null default 0")
    private float supPlanoInterno;

    @Column(columnDefinition = "float not null default 0")
    private float supAchurada;

    @Column(columnDefinition = "int not null default 0")
    private TipoNucleo tipo;

    @Column(columnDefinition = "float not null default 0")
    private float supGrandesAreas;

    @Column(columnDefinition = "float not null default 0")
    private float supSinRegularizar;

    @NotNull
    private String name;

    @Column(nullable = true)
    private String otrosNombres;

    @Column(columnDefinition = "float not null default 0")
    private float supParcelada;

    @Column(columnDefinition = "float not null default 0")
    private float supAcentamientoHumanoDelimitadaInterior;

    @Column(columnDefinition = "int not null default 0")
    private NucleoAgrarioType nucleoAgrarioType;

    @Column(columnDefinition = "float not null default 0")
    private float supReservadaCrecimiento;

    @Column(columnDefinition = "float not null default 0")
    private float supAcentamientoHumanoSinDelimitarInterior;

    @Column(nullable = true)
    private String claveCompuesta;

    @Column(columnDefinition = "float not null default 0")
    private float supExplotacionColectiva;

    @Column(columnDefinition = "float not null default 0")
    private float supUsoComun;

    @Column(nullable = true)
    private String folio;

    @Column(columnDefinition = "float not null default 0")
    private float supOtros;

    @Column(nullable = true)
    private Integer acciones;

    @Column(nullable = true)
    private Integer ejidatarios;

    @Column(nullable = true)
    private Integer posesionarios;

    @Column(columnDefinition = "float not null default 0")
    private float supTotalNucleo;

    @Column(nullable = true)
    private Integer avecindados;

    @Column(nullable = true)
    private AccionNucleo tipoAccionFormacion;

    //@NotNull
    @Basic
    @Temporal(TemporalType.DATE)
    private java.util.Date fechaDecretoFormacion;

    @Column(nullable = true)
    @Basic
    @Temporal(TemporalType.DATE)
    private java.util.Date fechaPublicacionFormacion;

    @OneToMany(mappedBy = "nucleoAgrario", cascade = CascadeType.ALL)
    @Size(max = 5)
    private Set<Documento> documents = new HashSet<>();

    @OneToOne(mappedBy = "nucleoAgrario", cascade = CascadeType.ALL)
    private Enlace enlace;

    @OneToOne(mappedBy = "nucleoAgrario", cascade = CascadeType.ALL)
    private Comisario comisario;

    @Column(columnDefinition = "int not null default 0")
    private boolean priority;

    //Temporary DATA
    @JoinColumn(name = "tempDocControl_id", unique = true)
    @OneToOne(cascade = CascadeType.ALL)
    private TempDocControl docsSummary;

    @Column(nullable = true)
    private Boolean hasComments;

    @Column(nullable = true)
    private String comments;

    //ClaveUnica varchar
    //Estado
    //Folio varchar
    //Fecha Inscripcion date
    //Municipio
    //Sup. Plano Interno float
    //Sup. Achurada float
    //TIPO
    //Grandes Areas float
    //Sup sin regularizar float
    //Nombre string
    //Otros nombres String
    //Sup Parcelada float
    //Sup Asent. Hum. Delimitado al Interior float
    //Clasificacion
    //Sup Reser. Crecimiento float
    //Sup Asent. Hum. Sin Delimitar al Interior float
    //Clave int
    //Sup Explot. Colectiva float
    //Sup Uso Comun float
    //Folio Ejidos y Comunidades varchar
    //Sup Otros float
    //Acciones int
    //Ejidatarios int
    //Posesionarios int
    //Sup. Total del Núcleo float
    //Avecindados int

    //GETTERS AND SETTERS


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Municipality getMunicipality() {
        return municipality;
    }

    public void setMunicipality(Municipality municipality) {
        this.municipality = municipality;
    }

    public String getClaveUnica() {
        return claveUnica;
    }

    public void setClaveUnica(String claveUnica) {
        this.claveUnica = claveUnica;
    }

    public String getFolioMatriz() {
        return folioMatriz;
    }

    public void setFolioMatriz(String folioMatriz) {
        this.folioMatriz = folioMatriz;
    }

    public Date getFechaInscripcion() {
        return fechaInscripcion;
    }

    public void setFechaInscripcion(Date fechaInscripcion) {
        this.fechaInscripcion = fechaInscripcion;
    }

    public float getSupPlanoInterno() {
        return supPlanoInterno;
    }

    public void setSupPlanoInterno(float supPlanoInterno) {
        this.supPlanoInterno = supPlanoInterno;
    }

    public float getSupAchurada() {
        return supAchurada;
    }

    public void setSupAchurada(float supAchurada) {
        this.supAchurada = supAchurada;
    }

    public TipoNucleo getTipo() {
        return tipo;
    }

    public void setTipo(TipoNucleo tipo) {
        this.tipo = tipo;
    }

    public float getSupGrandesAreas() {
        return supGrandesAreas;
    }

    public void setSupGrandesAreas(float supGrandesAreas) {
        this.supGrandesAreas = supGrandesAreas;
    }

    public float getSupSinRegularizar() {
        return supSinRegularizar;
    }

    public void setSupSinRegularizar(float supSinRegularizar) {
        this.supSinRegularizar = supSinRegularizar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOtrosNombres() {
        return otrosNombres;
    }

    public void setOtrosNombres(String otrosNombres) {
        this.otrosNombres = otrosNombres;
    }

    public float getSupParcelada() {
        return supParcelada;
    }

    public void setSupParcelada(float supParcelada) {
        this.supParcelada = supParcelada;
    }

    public float getSupAcentamientoHumanoDelimitadaInterior() {
        return supAcentamientoHumanoDelimitadaInterior;
    }

    public void setSupAcentamientoHumanoDelimitadaInterior(float supAcentamientoHumanoDelimitadaInterior) {
        this.supAcentamientoHumanoDelimitadaInterior = supAcentamientoHumanoDelimitadaInterior;
    }

    public NucleoAgrarioType getNucleoAgrarioType() {
        return nucleoAgrarioType;
    }

    public void setNucleoAgrarioType(NucleoAgrarioType nucleoAgrarioType) {
        this.nucleoAgrarioType = nucleoAgrarioType;
    }

    public float getSupReservadaCrecimiento() {
        return supReservadaCrecimiento;
    }

    public void setSupReservadaCrecimiento(float supReservadaCrecimiento) {
        this.supReservadaCrecimiento = supReservadaCrecimiento;
    }

    public float getSupAcentamientoHumanoSinDelimitarInterior() {
        return supAcentamientoHumanoSinDelimitarInterior;
    }

    public void setSupAcentamientoHumanoSinDelimitarInterior(float supAcentamientoHumanoSinDelimitarInterior) {
        this.supAcentamientoHumanoSinDelimitarInterior = supAcentamientoHumanoSinDelimitarInterior;
    }

    public String getClaveCompuesta() {
        return claveCompuesta;
    }

    public void setClaveCompuesta(String claveCompuesta) {
        this.claveCompuesta = claveCompuesta;
    }

    public float getSupExplotacionColectiva() {
        return supExplotacionColectiva;
    }

    public void setSupExplotacionColectiva(float supExplotacionColectiva) {
        this.supExplotacionColectiva = supExplotacionColectiva;
    }

    public float getSupUsoComun() {
        return supUsoComun;
    }

    public void setSupUsoComun(float supUsoComun) {
        this.supUsoComun = supUsoComun;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public float getSupOtros() {
        return supOtros;
    }

    public void setSupOtros(float supOtros) {
        this.supOtros = supOtros;
    }

    public int getAcciones() {
        return acciones;
    }

    public void setAcciones(int acciones) {
        this.acciones = acciones;
    }

    public int getEjidatarios() {
        return ejidatarios;
    }

    public void setEjidatarios(int ejidatarios) {
        this.ejidatarios = ejidatarios;
    }

    public int getPosesionarios() {
        return posesionarios;
    }

    public void setPosesionarios(int posesionarios) {
        this.posesionarios = posesionarios;
    }

    public float getSupTotalNucleo() {
        return supTotalNucleo;
    }

    public void setSupTotalNucleo(float supTotalNucleo) {
        this.supTotalNucleo = supTotalNucleo;
    }

    public int getAvecindados() {
        return avecindados;
    }

    public void setAvecindados(int avecindados) {
        this.avecindados = avecindados;
    }

    public AccionNucleo getTipoAccionFormacion() {
        return tipoAccionFormacion;
    }

    public void setTipoAccionFormacion(AccionNucleo tipoAccionFormacion) {
        this.tipoAccionFormacion = tipoAccionFormacion;
    }

    public Date getFechaDecretoFormacion() {
        return fechaDecretoFormacion;
    }

    public void setFechaDecretoFormacion(Date fechaDecretoFormacion) {
        this.fechaDecretoFormacion = fechaDecretoFormacion;
    }

    public Date getFechaPublicacionFormacion() {
        return fechaPublicacionFormacion;
    }

    public void setFechaPublicacionFormacion(Date fechaPublicacionFormacion) {
        this.fechaPublicacionFormacion = fechaPublicacionFormacion;
    }

    public Set<Documento> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<Documento> documents) {
        this.documents = documents;
    }

    public Enlace getEnlace() {
        return enlace;
    }

    public void setEnlace(Enlace enlace) {
        this.enlace = enlace;
    }

    public Comisario getComisario() {
        return comisario;
    }

    public void setComisario(Comisario comisario) {
        this.comisario = comisario;
    }

    public boolean isHasComments() {
        return hasComments;
    }

    public void setHasComments(boolean hasComments) {
        this.hasComments = hasComments;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public boolean isPriority() {
        return priority;
    }

    public void setPriority(boolean priority) {
        this.priority = priority;
    }

    public TempDocControl getDocsSummary() {
        return docsSummary;
    }

    public void setDocsSummary(TempDocControl docsSummary) {
        this.docsSummary = docsSummary;
    }


}
