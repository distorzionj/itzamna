package com.example.itzamna.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@DiscriminatorValue(value=RoleType.Values.COMISARIO)
public class Comisario extends Role {

    @OneToOne
    @JoinColumn(name = "nucleoAgrario_id", nullable = true)
    private NucleoAgrario nucleoAgrario;


    @Past(message = "fechaInicio has to be a past date")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;

    @Future(message = "fechaFinal has to be a future date")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    private Date fechaFinal;

    public Comisario() {

    }

    public NucleoAgrario getNucleoAgrario() {
        return nucleoAgrario;
    }

    public void setNucleoAgrario(NucleoAgrario nucleoAgrario) {
        this.nucleoAgrario = nucleoAgrario;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }
}
