package com.example.itzamna.model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "document")
public class Documento {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "nucleo_agrario_id", nullable = false)
    private NucleoAgrario nucleoAgrario;

    @NotNull
    private TipoDocumento tipo;

    @CreationTimestamp
    private Date creationDate;

    //User

}
