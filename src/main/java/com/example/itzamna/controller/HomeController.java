package com.example.itzamna.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.WritableResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

@Controller
public class HomeController {

    @Value("${spring.application.name}")
    String appName;

    @Value("gs://${gcs-resource-ines-bucket}/my-file.txt")
    private Resource gcsFile;

    @Value("gs://${gcs-resource-ines-bucket}/")
    private String bucketDir;

    @Autowired
    ResourceLoader resourceLoader;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("appName",appName);
        return "index";
    }

    @RequestMapping("/greetings")
    public String greetings(
            @RequestParam(name="name", required=false, defaultValue="World")
            String name, Model model){
        model.addAttribute("name", name);
        model.addAttribute("appName",appName);
        return "greetings";
    }

    @ResponseBody
    @RequestMapping(value = "/texttest", method = RequestMethod.GET)
    public String readGcsFile() throws IOException {
        return StreamUtils.copyToString(
                this.gcsFile.getInputStream(),
                Charset.defaultCharset()) + "\n";
    }

    @ResponseBody
    @RequestMapping(value = "/texttest2", method = RequestMethod.GET)
    String writeGcs() throws IOException {
        Resource x = resourceLoader.getResource(bucketDir + "test/");
        System.out.println(x.getFilename());
        try (OutputStream os = ((WritableResource) x).getOutputStream()) {
            os.write(0);
        }
        return "file was updated\n";
    }

}