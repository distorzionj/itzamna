package com.example.itzamna.controller;

import com.example.itzamna.dao.*;
import com.example.itzamna.model.*;
import com.example.itzamna.service.MailService;
import com.example.itzamna.service.dto.FormOneDTO;
import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.WritableResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@RestController
public class FormController {

    @Autowired
    private FormTokenRepository formTokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private NucleoAgrarioRepository nucleoAgrarioRepository;

    @Autowired
    private MailService mailService;

    @Autowired
    ResourceLoader resourceLoader;

    @Value("gs://${gcs-resource-ines-bucket}/")
    private String bucketDir;

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @RequestMapping(value = "/form/captureOne/{uuid}", method = RequestMethod.POST)
    public ResponseEntity<String> captureFormOne(@PathVariable UUID uuid, @RequestBody @Valid FormOneDTO formData) {
        LOGGER.debug("Request capture of [FormOne] for token: " + uuid.toString());
        Optional<FormToken> token = formTokenRepository.findById(uuid);
        if(token.isPresent()){
            if(token.get().getType().equals(FormTokenType.CAPTURE_FORM_ONE)) {
                formData.setFormToken(token.get());
                processFormOne(formData);

                Optional<FormToken> ineToken = formTokenRepository.findOneByEmailAndType(token.get().getEmail(), FormTokenType.CAPTURE_FORM_INE);
                if(!ineToken.isPresent()){
                    ineToken = Optional.of(new FormToken());
                    ineToken.get().setMailed(false);
                    ineToken.get().setNucleoAgrario(token.get().getNucleoAgrario());
                    ineToken.get().setEmail(token.get().getEmail());
                    ineToken.get().setType(FormTokenType.CAPTURE_FORM_INE);
                    formTokenRepository.save(ineToken.get());
                    LOGGER.info("Sending CaptureIne email for " + ineToken.get().getNucleoAgrario().getMunicipality().getName() + " -> " + ineToken.get().getNucleoAgrario().getName() + " to " + ineToken.get().getEmail());
                    mailService.sendCaptureIne(ineToken.get());
                }
                else {
                    LOGGER.info("RESending CaptureIne email for " + ineToken.get().getNucleoAgrario().getMunicipality().getName() + " -> " + ineToken.get().getNucleoAgrario().getName() + " to " + ineToken.get().getEmail());
                    mailService.sendCaptureIne(ineToken.get());
                }

                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body("{ \"SUCCESS\": \"Form saved correctly\" }");
            }
            else {
                LOGGER.info("[FormOne] requested, but is not a valid token: " + token.get().getId().toString());
                return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body("{ \"ERROR\": \"Token is not valid for FormOne\" }");
            }
        }
        else {
            return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body("{ \"ERROR\": \"Token not found\" }");
        }
    }

    @RequestMapping(value = "/form/captureIneFrente/{uuid}", method = RequestMethod.POST)
    public ResponseEntity<String> captureIneFrente(@PathVariable UUID uuid, @RequestParam("file") MultipartFile file){
        LOGGER.info("Request of upload [INE FRENTE] for token: " + uuid.toString());
        Optional<FormToken> token = formTokenRepository.findById(uuid);
        if(token.isPresent() && token.get().getType().equals(FormTokenType.CAPTURE_FORM_INE)){
            Optional<User> user = userRepository.findByEmail(token.get().getEmail());
            if(user.isPresent() && validateFileExtension(file)) {
                String rootDir = user.get().getId() + "/";
                String fileName = "FRENTE" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
                if(uploadFileToGCS(file, bucketDir + rootDir, fileName)){
                    user.get().setIneFrente(rootDir + fileName);
                    userRepository.save(user.get());
                    LOGGER.info("[INE FRENTE] uploaded for token: " + uuid.toString());
                    if(user.get().getIneFrente() != null && user.get().getIneReverso() != null && !formTokenRepository.findOneByEmailAndType(user.get().getEmail(), FormTokenType.NEXT_STEP).isPresent()){
                        FormToken newToken = new FormToken();
                        newToken.setType(FormTokenType.NEXT_STEP);
                        newToken.setEmail(user.get().getEmail());
                        newToken.setNucleoAgrario(token.get().getNucleoAgrario());
                        newToken.setMailed(false);
                        formTokenRepository.save(newToken);
                        LOGGER.info("Moving user to the next step: " + user.get().getEmail());
                    }
                    return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body("{ \"SUCCESS\": \"file uploaded\" }");
                }
                else {
                    return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body("{ \"ERROR\": \"UNKNOWN ERROR\" }");
                }
            }
            else{
                LOGGER.info("Cannot process upload request [INE FRENTE] NOT USER FOUND for token: " + uuid.toString());
                return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body("{ \"ERROR\": \"Please, contact administrator\" }");
            }
        }
        else{
            LOGGER.info("Request of upload [INE FRENTE] for INVALID token: " + uuid.toString());
            return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body("{ \"ERROR\": \"Token is not valid for uploading INE\" }");
        }
    }

    @RequestMapping(value = "/form/captureIneReverso/{uuid}", method = RequestMethod.POST)
    public ResponseEntity<String> captureIneReverso(@PathVariable UUID uuid, @RequestParam("file") MultipartFile file){
        LOGGER.info("Request of upload [INE REVERSO] for token: " + uuid.toString());
        Optional<FormToken> token = formTokenRepository.findById(uuid);
        if(token.isPresent() && token.get().getType().equals(FormTokenType.CAPTURE_FORM_INE)){
            Optional<User> user = userRepository.findByEmail(token.get().getEmail());
            if(user.isPresent() && validateFileExtension(file)) {
                String rootDir = user.get().getId() + "/";
                String fileName = "REVERSO" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
                if(uploadFileToGCS(file, bucketDir + rootDir, fileName)){
                    LOGGER.info("[INE REVERSO] uploaded for token: " + uuid.toString());
                    user.get().setIneReverso(rootDir + fileName);
                    userRepository.save(user.get());
                    if(user.get().getIneFrente() != null && user.get().getIneReverso() != null && !formTokenRepository.findOneByEmailAndType(user.get().getEmail(), FormTokenType.NEXT_STEP).isPresent()){
                        FormToken newToken = new FormToken();
                        newToken.setType(FormTokenType.NEXT_STEP);
                        newToken.setEmail(user.get().getEmail());
                        newToken.setNucleoAgrario(token.get().getNucleoAgrario());
                        newToken.setMailed(false);
                        formTokenRepository.save(newToken);
                        LOGGER.info("Moving user to the next step: " + user.get().getEmail());
                    }
                    return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body("{ \"SUCCESS\": \"file uploaded\" }");
                }
                else {
                    return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body("{ \"ERROR\": \"UNKNOWN ERROR\" }");
                }
            }
            else{
                LOGGER.info("Cannot process upload request [INE REVERSO] NOT USER FOUND for token: " + uuid.toString());
                return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body("{ \"ERROR\": \"Please, contact administrator\" }");
            }
        }
        else{
            LOGGER.info("Request of upload [INE REVERSO] for INVALID token: " + uuid.toString());
            return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body("{ \"ERROR\": \"Token is not valid for uploading INE\" }");
        }
    }

    @RequestMapping(value = "/form/{uuid}", method = RequestMethod.GET)
    public ModelAndView formRequest(@PathVariable UUID uuid) {
        LOGGER.debug("Request form for token: " + uuid.toString());
        Optional<FormToken> token = formTokenRepository.findById(uuid);
        if(token.isPresent()){
            LOGGER.debug("Token exists: " + uuid.toString());
            switch(token.get().getType()){
                case CONFIRM_MAIL:
                    LOGGER.info("Redirecting for mail activation for token: " + token.get().getId().toString());
                    return new ModelAndView("redirect:/form/mailconfirm/" + token.get().getId().toString());
                case CAPTURE_FORM_ONE:
                    LOGGER.info("Serving FormOne for token: " + token.get().getId().toString());
                    return new ModelAndView("formOne").addObject("formOneDTO", getFormOneDTO(token.get()));
                case CAPTURE_FORM_INE:
                    LOGGER.info("Serving FormINE for token: " + token.get().getId().toString());
                    return new ModelAndView("formIne");
                default:
                    LOGGER.info("No form or process found for Token: " + token.get().getId().toString());
                    return new ModelAndView("redirect:/form/tokenNotFound");
            }
        }
        else {
            LOGGER.debug("Token not found: " + uuid.toString());
            return new ModelAndView("redirect:/form/tokenNotFound");
        }
    }



    @RequestMapping(value = "/form/mailconfirm/{uuid}", method = RequestMethod.GET)
    public ResponseEntity<String> mailConfirm(@PathVariable UUID uuid) {
        LOGGER.debug("Mail confirm request for token: " + uuid.toString());
        try{
            Optional<FormToken> currentToken = formTokenRepository.findById(uuid);
            if(currentToken.isPresent()){
                if(formTokenRepository.findOneByEmailAndType(currentToken.get().getEmail(), FormTokenType.CAPTURE_FORM_ONE).isPresent()) {
                    FormToken captureToken = formTokenRepository.findOneByEmailAndType(currentToken.get().getEmail(), FormTokenType.CAPTURE_FORM_ONE).get();

                    LOGGER.info("Sending CaptureOne email for " + captureToken.getNucleoAgrario().getMunicipality().getName() + " -> " + captureToken.getNucleoAgrario().getName() + " to " + captureToken.getEmail());
                    mailService.sendCaptureOne(captureToken);
                    return ResponseEntity.ok("Tu correo ya ha sido confirmado con anterioridad, en breve recibirás los formatos y las instrucciones para la captura");
                }
                else {
                    FormToken captureToken = new FormToken(FormTokenType.CAPTURE_FORM_ONE);
                    captureToken.setEmail(currentToken.get().getEmail());
                    captureToken.setNucleoAgrario(currentToken.get().getNucleoAgrario());
                    captureToken.setMailed(false);
                    formTokenRepository.save(captureToken);

                    LOGGER.info("Mail confirmed for token: " + uuid.toString() + " with mail: " + currentToken.get().getEmail());
                    LOGGER.info("Sending CaptureOne email for " + captureToken.getNucleoAgrario().getMunicipality().getName() + " -> " + captureToken.getNucleoAgrario().getName() + " to " + captureToken.getEmail());
                    mailService.sendCaptureOne(captureToken);
                    return ResponseEntity.ok("Gracias por confirmar tu correo electrónico, en breve recibirás los formatos y las instrucciones para la captura");

                }
            }
            else return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body("{ \"ERROR\": \"Token not found\" }");
        } catch (IllegalArgumentException exception){
            return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body("{ \"ERROR\": \"No Token given\" }");
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @RequestMapping(value = "/form/tokenNotFound", method = RequestMethod.GET)
    public Map<String, String> formTokenNotFound(){
        Map<String, String> errors = new HashMap<>();
        errors.put("ERROR", "Token not found");
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @RequestMapping(value = "/form/formNotFound", method = RequestMethod.GET)
    public Map<String, String> formFormNotFound(){
        Map<String, String> errors = new HashMap<>();
        errors.put("ERROR", "No active form for your token");
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        LOGGER.debug("MethodArgumentNotValidException handler called");
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error ->
                errors.put(error.getField(), error.getDefaultMessage()));
        Gson gson = new Gson();
        LOGGER.debug(gson.toJson(errors));
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public Map<String, String> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        LOGGER.debug("MethodArgumentTypeMismatchException handler called");
        LOGGER.info("Cannot parse a requested token: " + ex.getCause().getMessage());
        Map<String, String> errors = new HashMap<>();
        errors.put(ex.getParameter().getParameterName(), ex.getCause().getMessage());
        Gson gson = new Gson();
        LOGGER.debug(gson.toJson(errors));
        return errors;
    }

    private void processFormOne(FormOneDTO form){
        User enlaceUser;
        Boolean firstTime = false;

        //ENLACE USER
        Optional<User> formUser = userRepository.findByEmail(form.getFormToken().getEmail());
        if(!formUser.isPresent()){
            LOGGER.debug("User " + form.getFormToken().getEmail() + " NOT found for the token, trying to find one. [CAPTURE ONE] token: " + form.getFormToken().getId());
            try{
                enlaceUser = form.getFormToken().getNucleoAgrario().getEnlace().getUser();
                LOGGER.debug("User " + form.getFormToken().getEmail() + " NOT found for the token. First time protocol activated. [CAPTURE ONE] token: " + form.getFormToken().getId());
            } catch (NullPointerException ex){
                enlaceUser = new User();
                firstTime = true;
            }
        }
        else{
            LOGGER.debug("User " + form.getFormToken().getEmail() + " found for the captureOne token: " + form.getFormToken().getId());
            enlaceUser = formUser.get();
        }

        enlaceUser.setEmail(form.getFormToken().getEmail());
        enlaceUser.setFirstName(form.getEnlaceFirstName());
        enlaceUser.setLastName(form.getEnlaceLastName());
        enlaceUser.setBirthday(form.getEnlaceBirthday());
        enlaceUser.setPhone(form.getEnlacePhone());
        enlaceUser.setAddress(form.getEnlaceAddress());
        //userRepository.save(enlaceUser);

        //ENLACE ROLE
        Enlace enlace;
        if(firstTime) enlace = new Enlace();
        else{
            enlace = form.getFormToken().getNucleoAgrario().getEnlace();
            if(enlace == null){
                LOGGER.info("DB role inconsistency, repair attempt. Role: ENLACE | NucleoAgrario: " + form.getFormToken().getNucleoAgrario());
                Optional<Role> tmpEnlace = enlaceUser.getRoles().stream().filter(role -> role.getRoleType().equals(RoleType.ENLACE) && ((Enlace) role).getNucleoAgrario().equals(form.getFormToken().getNucleoAgrario()) ).findFirst();
                if(tmpEnlace.isPresent()) enlace = (Enlace)tmpEnlace.get();
                else{
                    LOGGER.info("DB role inconsistency, unable to recover. Role: ENLACE | NucleoAgrario: " + form.getFormToken().getNucleoAgrario());
                    enlace = new Enlace();
                }
            }
        }

        enlace.setNucleoAgrario(form.getFormToken().getNucleoAgrario());
        enlaceUser.addRole(enlace);
        enlace.setUser(enlaceUser);
        form.getFormToken().getNucleoAgrario().setEnlace(enlace);


        userRepository.save(enlaceUser);
        nucleoAgrarioRepository.save(form.getFormToken().getNucleoAgrario());
        roleRepository.save(enlace);

        //COMISARIO USER
        User comisarioUser;

        if(firstTime) comisarioUser = new User();
        else comisarioUser = form.getFormToken().getNucleoAgrario().getComisario().getUser();

        if(comisarioUser == null){
            LOGGER.info("DB role inconsistency, unable to recover. Entity: USER -> COMISARIO | NucleoAgrario: " + form.getFormToken().getNucleoAgrario());
            comisarioUser = new User();
        }

        comisarioUser.setPhone(form.getComisarioPhone());
        comisarioUser.setBirthday(form.getComisarioBirthday());
        comisarioUser.setFirstName(form.getComisarioFirstName());
        comisarioUser.setLastName(form.getComisarioLastName());
        comisarioUser.setAddress(form.getComisarioAddress());

        //COMISARIO
        Comisario comisario;

        if(firstTime) comisario = new Comisario();
        else{
            comisario = form.getFormToken().getNucleoAgrario().getComisario();
            if(comisario == null){
                LOGGER.info("DB role inconsistency, repair attempt. Role: COMISARIO | NucleoAgrario: " + form.getFormToken().getNucleoAgrario());
                Optional<Role> tmpComisario = comisarioUser.getRoles().stream().filter(role -> role.getRoleType().equals(RoleType.COMISARIO) && ((Enlace) role).getNucleoAgrario().equals(form.getFormToken().getNucleoAgrario()) ).findFirst();
                if(tmpComisario.isPresent()) comisario = (Comisario)tmpComisario.get();
                else{
                    LOGGER.info("DB role inconsistency, unable to recover. Role: ENLACE | NucleoAgrario: " + form.getFormToken().getNucleoAgrario());
                    comisario = new Comisario();
                }
            }
        }

        comisario.setNucleoAgrario(form.getFormToken().getNucleoAgrario());
        comisario.setFechaFinal(form.getComisarioFechaFinal());
        comisario.setFechaInicio(form.getComisarioFechaInicio());
        comisario.setUser(comisarioUser);
        form.getFormToken().getNucleoAgrario().setComisario(comisario);
        comisarioUser.addRole(comisario);

        userRepository.save(comisarioUser);
        nucleoAgrarioRepository.save(form.getFormToken().getNucleoAgrario());
        roleRepository.save(comisario);
    }

    private FormOneDTO getFormOneDTO(FormToken token){
        FormOneDTO formDTOforToken = new FormOneDTO();
        Optional<User> formUser = userRepository.findByEmail(token.getEmail());


        if(!formUser.isPresent()){
            LOGGER.debug("Form user NOT found, returning Empty form for token: " + token.getId());
            return formDTOforToken;
        }
        else {
            LOGGER.debug("Form user found, baking a DTO for token: " + token.getId());
            LOGGER.debug("User: " + formUser.get().getEmail());
            Enlace enlace = token.getNucleoAgrario().getEnlace();
            Comisario comisario = token.getNucleoAgrario().getComisario();
            User comisarioUser = comisario.getUser();

            formDTOforToken.setEnlaceAddress(formUser.get().getAddress());
            formDTOforToken.setEnlaceBirthday(formUser.get().getBirthday());
            formDTOforToken.setEnlaceFirstName(formUser.get().getFirstName());
            formDTOforToken.setEnlaceLastName(formUser.get().getLastName());
            formDTOforToken.setEnlacePhone(formUser.get().getPhone());

            formDTOforToken.setComisarioAddress(comisarioUser.getAddress());
            formDTOforToken.setComisarioBirthday(comisarioUser.getBirthday());
            formDTOforToken.setComisarioFirstName(comisarioUser.getFirstName());
            formDTOforToken.setComisarioLastName(comisarioUser.getLastName());
            formDTOforToken.setComisarioPhone(comisarioUser.getPhone());
            formDTOforToken.setComisarioFechaFinal(comisario.getFechaFinal());
            formDTOforToken.setComisarioFechaInicio(comisario.getFechaInicio());

            return formDTOforToken;
        }
    }

    private boolean uploadFileToGCS(MultipartFile file, String rootDir, String fileName){
        Resource root = resourceLoader.getResource(rootDir);
        Resource newFile = resourceLoader.getResource(rootDir + fileName);

        if(!root.exists()){
            try (OutputStream os = ((WritableResource) root).getOutputStream()) {
                os.write(0);
            } catch (IOException ex){
                LOGGER.error("Cannot create a GCS folder: " + rootDir);
                return false;
            }
        }
        else LOGGER.debug("GS Folder already exists");
        try{
            OutputStream os = ((WritableResource) newFile).getOutputStream();
            ByteStreams.copy(file.getInputStream(), os);
            file.getInputStream().close();
            os.flush();
            os.close();
        } catch (IOException ex){
            LOGGER.error("Cannot upload file to GCS: " + rootDir + fileName);
            return false;
        }

        return true;
    }

    private boolean validateFileExtension(MultipartFile file){
        String fileName = file.getOriginalFilename().toUpperCase();
        boolean extension = fileName.endsWith(".JPG")
                || fileName.endsWith(".JPEG")
                || fileName.endsWith(".PNG")
                || fileName.endsWith(".SVG")
                || fileName.endsWith(".BPM")
                || fileName.endsWith(".GIF")
                || fileName.endsWith(".TIF")
                || fileName.endsWith(".TIFF")
                || fileName.endsWith(".RAW")
                || fileName.endsWith(".NEF")
                || fileName.endsWith(".CR2")
                || fileName.endsWith(".CRW")
                || fileName.endsWith(".DNG")
                || fileName.endsWith(".SRF")
                || fileName.endsWith(".ARW");
        return extension;

    }

}
