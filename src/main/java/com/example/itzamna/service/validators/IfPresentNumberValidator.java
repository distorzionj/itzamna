package com.example.itzamna.service.validators;

import org.apache.tomcat.util.buf.StringUtils;
import org.thymeleaf.util.NumberUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IfPresentNumberValidator implements ConstraintValidator<IfPresentNumber, String> {
    private Pattern pattern;
    private Matcher matcher;
    private static final String NUMBER_PATTERN = "^([1-9][0-9]*)$";

    @Override
    public void initialize(final IfPresentNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(final String number, final ConstraintValidatorContext context) {
        if(number == null || number.equals("")) return true; //Si no esta presente o no tiene datos, pasa la prueba
        else return isNumeric(number);
    }

    private boolean isNumeric(final String number) {
        pattern = Pattern.compile(NUMBER_PATTERN);
        matcher = pattern.matcher(number);
        return matcher.matches();
    }
}

