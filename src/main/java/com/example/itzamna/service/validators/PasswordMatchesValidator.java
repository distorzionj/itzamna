package com.example.itzamna.service.validators;

import com.example.itzamna.service.dto.UserDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {

    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext constraintValidatorContext) {
        UserDTO user = (UserDTO) obj;
        if(user.getPassword() == null || user.getMatchingPassword() == null) return false;
        else return user.getPassword().equals(user.getMatchingPassword());
    }
}
