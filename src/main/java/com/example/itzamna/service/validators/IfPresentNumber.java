package com.example.itzamna.service.validators;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({ TYPE, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy =  IfPresentNumberValidator.class)
@Documented
public @interface IfPresentNumber {
    String message() default "Is present but not a number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
