package com.example.itzamna.service;

import com.example.itzamna.dao.*;
import com.example.itzamna.model.*;
import com.example.itzamna.service.dto.FormOneDTO;
import com.example.itzamna.service.exception.FormTokenNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FormService implements IFormService {

    @Autowired
    private FormTokenRepository formTokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ComisarioRepository comisarioRepository;

    @Autowired
    private EnlaceRepository enlaceRepository;

    @Autowired
    private NucleoAgrarioRepository nucleoAgrarioRepository;

    @Override
    public List<Object> formOneDataCapture(FormOneDTO formData) throws FormTokenNotFoundException {
        FormToken token = formData.getFormToken();
        if(token == null) {
            throw new FormTokenNotFoundException("Form Token not found, invalid or expired");
        }
        NucleoAgrario nucleoAgrario  = token.getNucleoAgrario();
        Optional<User> enlaceUser = userRepository.findByEmail(formData.getEnlaceEmail());
        Enlace enlace;
        User comisarioUser;
        Comisario comisario = nucleoAgrario.getComisario();
        if(enlaceUser.isPresent()){ //Ya ha sido enviado previo, modificar el user
            enlace = (Enlace) enlaceUser.get().getRoles().stream().filter(Enlace.class::isInstance).findFirst().get();
        }
        else {
            enlaceUser = Optional.of(new User());
            enlace = new Enlace();
        }

        enlaceUser.get().setEmail(formData.getEnlaceEmail());
        enlaceUser.get().setFirstName(formData.getEnlaceFirstName());
        enlaceUser.get().setLastName(formData.getEnlaceLastName());
        enlaceUser.get().setBirthday(formData.getEnlaceBirthday());

        enlace.setUser(enlaceUser.get());
        enlace.setNucleoAgrario(nucleoAgrario);

        if(comisario != null){ //Ya ha sido enviado previo, modificar el user
            comisarioUser = comisario.getUser();
        }
        else { //Nueva recepcion
            comisario = new Comisario();
            comisarioUser = new User();
        }

        comisarioUser.setEmail(formData.getComisarioEmail());
        comisarioUser.setFirstName(formData.getComisarioFirstName());
        comisarioUser.setLastName(formData.getComisarioLastName());
        comisarioUser.setBirthday(formData.getComisarioBirthday());

        comisario.setUser(comisarioUser);
        comisario.setNucleoAgrario(nucleoAgrario);
        comisario.setFechaInicio(formData.getComisarioFechaInicio());
        comisario.setFechaFinal(formData.getComisarioFechaFinal());

        //SAVE BLOCK
        userRepository.save(enlaceUser.get());
        enlaceRepository.save(enlace);
        userRepository.save(comisarioUser);
        comisarioRepository.save(comisario);

        nucleoAgrario.setHasComments(formData.getNucleaAgrarioHasNotes());
        nucleoAgrario.setComments(formData.getNucleAgrarioNotes());
        nucleoAgrarioRepository.save(nucleoAgrario);


        return null;
    }

}
