package com.example.itzamna.service;

import com.example.itzamna.dao.FormTokenRepository;
import com.example.itzamna.model.FormToken;
import com.example.itzamna.service.dto.EmailDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.Access;
import java.io.UnsupportedEncodingException;

@Service("mailService")
public class MailService implements IMailService {

    @Autowired
    JavaMailSender mailSender;

    @Autowired
    FormTokenRepository formTokenRepository;

    @Override
    public void sendEmail(EmailDTO email) throws MessagingException, UnsupportedEncodingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();


        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

        mimeMessageHelper.setSubject(email.getMailSubject());
        mimeMessageHelper.setFrom(new InternetAddress(email.getMailFrom(), "Fondo Mixto Privado"));
        mimeMessageHelper.setTo(email.getMailTo());
        mimeMessageHelper.setText(email.getMailContent(), true);

        mailSender.send(mimeMessageHelper.getMimeMessage());

    }

    public void sendCaptureOne(FormToken token){
        EmailDTO mail = new EmailDTO();

        mail.setMailFrom("fondo.mixto.pa@gmail.com");
        mail.setMailTo(token.getEmail());
        mail.setMailSubject("Fondo Mixto Privado - Formato de Enlace y Comisario");
        String mailContent = new StringBuilder()
                .append("Hola,<br /><br />")
                .append("Este es un correo de seguimiento del Fondo Mixto Privado de los Pueblos originarios.<br /><br />")
                .append("Gracias por haber confirmado tu correo electrónico. Para poder avanzar con los requisitos solicitados, es necesario que nos ayudes llenando el formato que se encuentra en el enlace al final del correo.<br /><br />")
                .append("Instrucciones:<br /><br />")
                .append("<ul><li><b>Para guardar, es necesario dar click en el boton de guardar (color verde) al final del formato</b> Un letrero en color verde (que desaparece lentamente) lo confirma</li>")
                .append("<li><b>Los campos del enlace (con asterisco rojo), son estrictamente necesarios</b></li>")
                .append("<li><b>Los datos del comisario pueden ser enviados por partes</b></li>")
                .append("<li><b>Cuando los datos sean guardados con éxito, aparecerá un letrero en color verde, que indicará que así sucedió</b></li>")
                .append("<li>Para enviar los datos por partes, solo es necesario dar click en guardar, los datos serán guardados y la próxima vez que retomes el formulario serán recuperados automáticamente</li>")
                .append("<li>Para modificar los datos ya enviados, basta con cambiar la información, y volver a dar click en el botón de guardar</li>")
                .append("<li><b>El formato esta configurado para aceptar solo datos válidos</b>. Si crees que existe algún error con la validación de un dato, por favor reportalo con tu monitor de grupo</li></ul><br /><br />")
                .append("Podrás encontrar el formato, dando click en <a href='http://itzamna.appspot.com/form/" + token.getId().toString() + "'>http://itzamna.appspot.com/form/" + token.getId().toString() + "</a><br /><br />")
                .toString();
        mail.setMailContent(mailContent);
        try{
            sendEmail(mail);
            token.setMailed(true);
            formTokenRepository.save(token);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendCaptureIne(FormToken token) {
        EmailDTO mail = new EmailDTO();

        mail.setMailFrom("fondo.mixto.pa@gmail.com");
        mail.setMailTo(token.getEmail());
        mail.setMailSubject("Fondo Mixto Privado - Formato de INE");
        String mailContent = new StringBuilder()
                .append("Hola,<br /><br />")
                .append("Este es un correo de seguimiento del Fondo Mixto Privado de los Pueblos originarios.<br /><br />")
                .append("El siguiente paso es poder validar tu INE, para ello, encontrarás el formato en el enlace al final del correo (en color azul)")
                .append("Instrucciones:<br /><br />")
                .append("<ul><li>El botón de guardar estará inicialmente deshabilitado</li>")
                .append("<li><b>EL BOTÓN DE GUARDAR SE HABILITARÁ UNA VEZ QUE LAS FOTOS SEAN SELECCIONADAS</b></li>")
                .append("<li><b>Para guardar, es necesario dar click en el boton de guardar (color verde) al final del formato</b> Un letrero en color verde (que desaparece lentamente) lo confirma</li>")
                .append("<li><b>Los campos del enlace (con asterisco rojo), son estrictamente necesarios</b></li>")
                .append("<li><b>Cuando los datos sean guardados con éxito, aparecerá un letrero en color verde, que indicará que así sucedió</b></li>")
                .append("<li>Para modificar los datos ya enviados, basta con abrir el enlace nuevamente y realizar el proceso</li></ul>")
                .append("<b>Podrás encontrar el formato, dando click en <a href='http://itzamna.appspot.com/form/" + token.getId().toString() + "'>http://itzamna.appspot.com/form/" + token.getId().toString() + "</a></b><br /><br />")
                .toString();
        mail.setMailContent(mailContent);
        try{
            sendEmail(mail);
            token.setMailed(true);
            formTokenRepository.save(token);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValidEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }
}

