package com.example.itzamna.service;

import com.example.itzamna.service.dto.FormOneDTO;
import com.example.itzamna.service.exception.FormTokenNotFoundException;


import java.util.List;

public interface IFormService {

    List<Object> formOneDataCapture(FormOneDTO formData) throws FormTokenNotFoundException;

}
