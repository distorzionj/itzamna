package com.example.itzamna.service.exception;

public class FormTokenNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FormTokenNotFoundException() {
        super();
    }

    public FormTokenNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FormTokenNotFoundException(final String message) {
        super(message);
    }

    public FormTokenNotFoundException(final Throwable cause) {
        super(cause);
    }

}