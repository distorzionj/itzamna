package com.example.itzamna.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.util.Date;

//@PasswordMatches
public class UserDTO {

    @NotBlank(message = "firstName required")
    private String firstName;

    @NotBlank(message = "lastName required")
    private String lastName;

    @NotBlank(message = "password required")
    private String password;

    @NotBlank(message = "matchingPassword required")
    private String matchingPassword;

    @NotBlank(message = "email required")
    //@ValidEmail
    private String email;

    @Past(message = "birthday has to be a past date")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @NotBlank(message = "birthday required")
    private Date birthday;

    private String phoneNumber;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
