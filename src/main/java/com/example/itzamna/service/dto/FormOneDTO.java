package com.example.itzamna.service.dto;

import com.example.itzamna.model.FormToken;
import com.example.itzamna.service.validators.IfPresentNumber;
import com.example.itzamna.service.validators.ValidEmail;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class FormOneDTO implements Serializable {

    private FormToken formToken;

    @NotBlank(message = "enlaceFirstName required")
    private String enlaceFirstName;

    //@NotBlank(message = "comisarioFirstName required")
    private String comisarioFirstName;

    @NotBlank(message = "enlaceLastName required")
    private String enlaceLastName;

    //@NotBlank(message = "comisarioLastName required")
    private String comisarioLastName;

    //@NotBlank(message = "enlaceEmail required")
    //@ValidEmail
    private String enlaceEmail;

    //@ValidEmail
    private String comisarioEmail;

    @Past(message = "enlaceBirthday has to be a past date")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @NotNull(message = "enlaceBirthday required")
    private Date enlaceBirthday;

    @Past(message = "comisarioBirthday has to be a past date")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date comisarioBirthday;

    @Length(min = 10, max = 10)
    @NotBlank(message = "enlacePhone required")
    @IfPresentNumber(message = "Present but not a number")
    private String enlacePhone;

    //@Length(min = 10, max = 10)
    @IfPresentNumber(message = "Present but not a number")
    private String comisarioPhone;

    @Past(message = "comisarioFechaInicio has to be a past date")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date comisarioFechaInicio;

    @FutureOrPresent(message = "comisarioFechaFinal has to be a future date")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date comisarioFechaFinal;

    @NotBlank(message = "enlaceAddress required")
    private String enlaceAddress;

    private String comisarioAddress;

    private Boolean nucleaAgrarioHasNotes;

    private String nucleAgrarioNotes;

    public FormOneDTO() {
    }

    public FormToken getFormToken() {
        return formToken;
    }

    public void setFormToken(FormToken formToken) {
        this.formToken = formToken;
    }

    public String getEnlaceFirstName() {
        return enlaceFirstName;
    }

    public void setEnlaceFirstName(String enlaceFirstName) {
        this.enlaceFirstName = enlaceFirstName;
    }

    public String getComisarioFirstName() {
        return comisarioFirstName;
    }

    public void setComisarioFirstName(String comisarioFirstName) {
        this.comisarioFirstName = comisarioFirstName;
    }

    public String getEnlaceLastName() {
        return enlaceLastName;
    }

    public void setEnlaceLastName(String enlaceLastName) {
        this.enlaceLastName = enlaceLastName;
    }

    public String getComisarioLastName() {
        return comisarioLastName;
    }

    public void setComisarioLastName(String comisarioLastName) {
        this.comisarioLastName = comisarioLastName;
    }

    public String getEnlaceEmail() {
        return enlaceEmail;
    }

    public void setEnlaceEmail(String enlaceEmail) {
        this.enlaceEmail = enlaceEmail;
    }

    public String getComisarioEmail() {
        return comisarioEmail;
    }

    public void setComisarioEmail(String comisarioEmail) {
        this.comisarioEmail = comisarioEmail;
    }

    public Date getEnlaceBirthday() {
        return enlaceBirthday;
    }

    public void setEnlaceBirthday(Date enlaceBirthday) {
        this.enlaceBirthday = enlaceBirthday;
    }

    public Date getComisarioBirthday() {
        return comisarioBirthday;
    }

    public void setComisarioBirthday(Date comisarioBirthday) {
        this.comisarioBirthday = comisarioBirthday;
    }

    public String getEnlacePhone() {
        return enlacePhone;
    }

    public void setEnlacePhone(String enlacePhone) {
        this.enlacePhone = enlacePhone;
    }

    public String getComisarioPhone() {
        return comisarioPhone;
    }

    public void setComisarioPhone(String comisarioPhone) {
        this.comisarioPhone = comisarioPhone;
    }

    public Date getComisarioFechaInicio() {
        return comisarioFechaInicio;
    }

    public void setComisarioFechaInicio(Date comisarioFechaInicio) {
        this.comisarioFechaInicio = comisarioFechaInicio;
    }

    public Date getComisarioFechaFinal() {
        return comisarioFechaFinal;
    }

    public void setComisarioFechaFinal(Date comisarioFechaFinal) {
        this.comisarioFechaFinal = comisarioFechaFinal;
    }

    public Boolean getNucleaAgrarioHasNotes() {
        return nucleaAgrarioHasNotes;
    }

    public void setNucleaAgrarioHasNotes(Boolean nucleaAgrarioHasNotes) {
        this.nucleaAgrarioHasNotes = nucleaAgrarioHasNotes;
    }

    public String getNucleAgrarioNotes() {
        return nucleAgrarioNotes;
    }

    public void setNucleAgrarioNotes(String nucleAgrarioNotes) {
        this.nucleAgrarioNotes = nucleAgrarioNotes;
    }

    public String getEnlaceAddress() {
        return enlaceAddress;
    }

    public void setEnlaceAddress(String enlaceAddress) {
        this.enlaceAddress = enlaceAddress;
    }

    public String getComisarioAddress() {
        return comisarioAddress;
    }

    public void setComisarioAddress(String comisarioAddress) {
        this.comisarioAddress = comisarioAddress;
    }

}
