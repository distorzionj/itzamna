package com.example.itzamna.service;

import com.example.itzamna.model.FormToken;
import com.example.itzamna.service.dto.EmailDTO;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public interface IMailService {

    public void sendEmail(EmailDTO email) throws MessagingException, UnsupportedEncodingException;
    public void sendCaptureOne(FormToken token);
    public void sendCaptureIne(FormToken token);
    public boolean isValidEmail(String email);

}
