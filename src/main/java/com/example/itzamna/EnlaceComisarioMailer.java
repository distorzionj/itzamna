package com.example.itzamna;

import com.example.itzamna.dao.FormTokenRepository;
import com.example.itzamna.model.FormToken;
import com.example.itzamna.model.FormTokenType;
import com.example.itzamna.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


import javax.transaction.Transactional;
import java.util.List;

@Component
@Transactional
public class EnlaceComisarioMailer implements ApplicationListener<ApplicationReadyEvent> {

    private boolean processFlag = false;
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    MailService mailService;

    @Autowired
    FormTokenRepository formTokenRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        if(processFlag) {
            List<FormToken> tokens = formTokenRepository.findAllByType(FormTokenType.CAPTURE_FORM_ONE);
            tokens.forEach(token -> {
                if (!token.isMailed()) {
                    LOGGER.debug("Sending CaptureOne email for " + token.getNucleoAgrario().getMunicipality().getName() + " -> " + token.getNucleoAgrario().getName() + " to " + token.getEmail());
                    mailService.sendCaptureOne(token);
                } else {
                    LOGGER.debug("CaptureOne email already sent for " + token.getNucleoAgrario().getMunicipality().getName() + " -> " + token.getNucleoAgrario().getName() + " to " + token.getEmail());
                }
            });
        }
    }


}
